<?php


/**
 * Implements hook_schema()
 */
function injapi_example_schema() {
  // Unfortunately this is still necessary. Injapi can't do it for you.
  return injapi_get_schema('injapi_example');
}


/**
 * Implements hook_injapi_schema()
 */
function injapi_example_injapi_schema($schema) {

  // Let's install a clone of the comment table.
  $table = $schema->table('injapi_comment');

  // Primary key
  $table->field_id('cid');

  // Foreign keys
  $table->field_idForeign('pid');
  $table->field_idForeign('nid', 'node', 'nid', 'comment_node');
  $table->field_idForeign('uid', 'users', 'uid', 'comment_author');

  // Data fields
  $table->field_varchar('subject', 64)->required(TRUE, '');
  $table->field_varchar('hostname', 128)->required(TRUE, '');
  $table->field_seconds('created')->required(TRUE, 0);
  $table->field_seconds('changed')->required(TRUE, 0);
  $table->field_unsigned('status', 'tiny')->required(TRUE, 1);
  $table->field_varchar('thread', 255)->required();
  $table->field_varchar('name', 60);
  $table->field_varchar('mail', 64);
  $table->field_varchar('homepage');
  $table->field_varchar('language', 12)->required(TRUE, '');

  // Field descriptions
  $table->describeFields(array(

    // Id columns
    'cid' => 'Primary Key: Unique comment ID.',
    'pid' => 'The {comment}.cid to which this comment is a reply. If set to 0, this comment is not a reply to an existing comment.',
    'nid' => 'The {node}.nid to which this comment is a reply.',
    'uid' => 'The {users}.uid who authored the comment. If set to 0, this comment was created by an anonymous user.',

    // Data columns
    'subject' => 'The comment title.',
    'hostname' => "The author's host name.",
    'created' => 'The time that the comment was created, as a Unix timestamp.',
    'changed' => 'The time that the comment was last edited, as a Unix timestamp.',
    'status' => 'The published status of a comment. (0 = Not Published, 1 = Published)',
    'thread' => "The vancode representation of the comment's place in a thread.",
    'name' => "The comment author's name. Uses {users}.name if the user is logged in, otherwise uses the value typed into the comment form.",
    'mail' => "The comment author's e-mail address from the comment form, if user is anonymous, and the 'Anonymous users may/must leave their contact information' setting is turned on.",
    'homepage' => "The comment author's home page address from the comment form, if user is anonymous, and the 'Anonymous users may/must leave their contact information' setting is turned on.",
    'language' => 'The {languages}.language of this comment.',
  ));

  // Indexes
  $table->index('comment_status_pid', array('pid', 'status'));
  $table->index('comment_num_new', array('nid', 'status', 'created', 'cid', 'thread'));
  $table->index('comment_uid', array('uid'));
  $table->index('comment_nid_language', array('nid', 'language'));
  $table->index('comment_created', array('created'));
}


function injapi_example_uninstall_() {
  drupal_uninstall_schema('injapi_example');
}
